import QuizService from "./QuizService";

describe('QuizService',  () => {
  describe('createQuiz', () => {
    it ('should return a new quiz from API response.', async () => {
      const spyFetch = jest.spyOn(global, 'fetch').mockImplementation(() => {
        return new Promise((resolve, reject) => {
          resolve({
            ok: true,
            json: () => {
              return {
                "results": [
                  {
                    "gender": "female",
                    "name": {
                      "title": "miss",
                      "first": "susie",
                      "last": "gonzalez"
                    },
                    "email": "susie.gonzalez@example.com",
                    "dob": {
                      "date": "1966-07-31T05:56:20Z",
                      "age": 53
                    },
                    "login": {
                      "uuid": "id1"
                    },
                    "registered": {
                      "date": "2002-06-03T07:12:33Z",
                      "age": 17
                    },
                    "phone": "017687 88535",
                    "cell": "0764-938-049",
                    "id": {
                      "name": "NINO",
                      "value": "SK 78 64 19 M"
                    },
                    "picture": {
                      "large": "https:\/\/randomuser.me\/api\/portraits\/women\/12.jpg",
                      "medium": "https:\/\/randomuser.me\/api\/portraits\/med\/women\/12.jpg",
                      "thumbnail": "https:\/\/randomuser.me\/api\/portraits\/thumb\/women\/12.jpg"
                    },
                    "nat": "GB"
                  },
                  {
                    "gender": "female",
                    "name": {
                      "title": "miss",
                      "first": "emilie",
                      "last": "s\u00f8rensen"
                    },
                    "login": {
                      "uuid": "id2"
                    },
                    "email": "emilie.s\u00f8rensen@example.com",
                    "dob": {
                      "date": "1991-11-03T10:53:45Z",
                      "age": 27
                    },
                    "registered": {
                      "date": "2008-09-10T09:07:53Z",
                      "age": 10
                    },
                    "phone": "49110972",
                    "cell": "75818032",
                    "id": {
                      "name": "CPR",
                      "value": "709301-8549"
                    },
                    "picture": {
                      "large": "https:\/\/randomuser.me\/api\/portraits\/women\/73.jpg",
                      "medium": "https:\/\/randomuser.me\/api\/portraits\/med\/women\/73.jpg",
                      "thumbnail": "https:\/\/randomuser.me\/api\/portraits\/thumb\/women\/73.jpg"
                    },
                    "nat": "DK"
                  }
                ],
              }
            }
        })
        })
      });

      const quiz = await QuizService.createQuiz();

      expect(quiz.users).toEqual({
        "id1": {
          id: "id1",
          name: "Susie Gonzalez",
          picture: 'https://randomuser.me/api/portraits/women/12.jpg'
        },
        'id2': {
          id: 'id2',
          name: 'Emilie Sorensen',
          picture: 'https://randomuser.me/api/portraits/women/73.jpg'
        }
      });

      expect(quiz.users[quiz.questions[0]]).toBeTruthy();
      expect(quiz.position).toBe(0);
    });
  })
});
