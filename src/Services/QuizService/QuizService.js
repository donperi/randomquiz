import sampleSize from 'lodash/sampleSize';
import startCase from 'lodash/startCase';

export default {
  async createQuiz(limit = 100) {
    try {
      const response = await fetch(`https://randomuser.me/api/?results=${limit}&nat=us,ca,gb`);
      const results = await response.json();

      const quizObject = {};
      quizObject.users = results.results.reduce((carry, user) => {
          const id = user.login.uuid;
          carry[id] = {
            id,
            name: `${startCase(user.name.first)} ${startCase(user.name.last)}`,
            picture: user.picture.large
          };
          return carry;
        }, {});

      quizObject.questions = sampleSize(Object.keys(quizObject.users), Object.keys(quizObject.users).length / 2);
      quizObject.answers = {};
      quizObject.position = 0;

      return quizObject;
    } catch (e) {
      console.error(e);
    }
  }
}
