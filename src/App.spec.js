import React from 'react';
import ReactDOM from 'react-dom';

import '@testing-library/jest-dom/extend-expect'
import {render, fireEvent, wait} from '@testing-library/react';
import App from './App';
import QuizService from "./Services/QuizService/QuizService";

it ('should create a Quiz', async () => {
  const spy = jest.spyOn(QuizService, 'createQuiz');
  spy.mockImplementation(() => {
    return Promise.resolve({
      users: { 1: { id: 1, name: '1', picture: { large: '1' } }, 2: { id: 2, picture: { large: '2' }}},
      questions: [1,2],
      position: 0,
      answers: {}
    });
  })

  const { getByText, findByText } = render(<App />);
  fireEvent.click(getByText(/start quiz/i));

  await findByText(/initializing/i);
  await wait(() => {
    expect(spy).toHaveBeenCalledWith(100);
  });

  expect(getByText(/question 1 out of 2/i)).toBeTruthy()
  expect(getByText(/submit/i)).toBeTruthy()
});
