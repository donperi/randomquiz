export class Quiz {
  constructor(quiz) {
    Object.keys(quiz).forEach((key) => {
      this[key] = quiz[key];
    });
  }

  getCurrentQuestion() {
    return this.users[this.questions[this.position]];
  }

  getOptions() {
    return Object.keys(this.users).map((userId) => {
      return { value: userId, label: this.users[userId].name };
    })
  }

  getCompletionPercent() {
    return ((this.position + 1 ) * 100)  / Object.keys(this.questions).length
  }

  correctAnswers() {
    return Object.values(this.answers).filter((item) => item).length;
  }

  failedAnswers() {
    return Object.values(this.answers).filter((item) => !item).length;
  }

  isFailed() {
    if (!this.answers) {
      return false;
    }

    return this.failedAnswers() >= Math.ceil((this.questions.length * 21) / 100);
  }

  isPassed() {
    if (!this.answers) {
      return false;
    }

    return this.correctAnswers() >= Math.floor((this.questions.length * 80) / 100);
  }

  isComplete() {
    if (!this.answers) {
      return false;
    }

    return Object.keys(this.answers).length === Object.keys(this.questions).length;
  }

  accuracyPercentage() {
    return this.correctAnswers() * 100 / Object.keys(this.answers).length;
  }
}
