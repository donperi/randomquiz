import {findByText, fireEvent, getByText} from "@testing-library/dom";

const KEY_DOWN = 40;
// Select an item from a React Select dropdown given a the id container and choice label.
export async function selectItem(container, id, choice) {
  const selectContainer = container.querySelector(`#${id}`);

  fireEvent.focus(selectContainer.querySelector('input'));
  fireEvent.keyDown(selectContainer.querySelector('input'), {
    keyCode: KEY_DOWN,
  });

  await findByText(selectContainer, choice);
  fireEvent.click(getByText(selectContainer, choice));
}

