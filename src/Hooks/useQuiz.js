import useLocalStorage from "./useLocalStorage";
import {Quiz} from "../Models/Quiz";

export default function useQuiz() {
  const [quiz, setQuiz] = useLocalStorage('quiz', null);
  const quizObject = quiz ? new Quiz(quiz) : null;

  return [quizObject , setQuiz];
}
