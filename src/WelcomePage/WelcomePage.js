import React from 'react';

function WelcomePage({ onStartClick, isLoading,  error}) {
  return (
    <div className="Welcome container mt-5">
      <h1 className="text-center mb-5">FXFamily Member Quiz</h1>
      {error && <div className="alert alert-danger mb-5">{error}</div>}
      <p>
        Test your knowledge of our #BestCoworkers' names using this picture quiz! You'll be shown 50 different randomized profile images and asked to correctly identify at least 40. We trust that you will not use any references during the quiz!
      </p>
      <p>
        Looking for ways to practice? Follow these ProTips!
        Study using the LiveBoard by selecting someone's name to see their profile image. See that person on FXCampus? Greet them using their first name!
        Link Liveboard to <a href="https://operationsfx.com/liveboard" target="_blank" rel="noreferrer noopener">https://operationsfx.com/liveboard</a>
      </p>

      <button onClick={onStartClick} disabled={isLoading} className="btn-block btn-primary btn-lg">
        {isLoading ? 'Initializing Quiz!' : 'Start Quiz!'}
      </button>
    </div>
  )
}

export default WelcomePage;
