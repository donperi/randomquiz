import React from 'react';
import './App.css';
import WelcomePage from "./WelcomePage/WelcomePage";
import QuizForm from "./QuizForm/QuizForm";
import useQuiz from "./Hooks/useQuiz";
import QuizService from "./Services/QuizService/QuizService";

function App() {
  const [quiz, setQuiz] = useQuiz();
  const [isCreatingQuiz, setIsCreatingQuiz] = React.useState(false);
  const [createError, setCreateError] = React.useState(false);

  const searchParams = new URLSearchParams(window.location.search.substring(1));
  const showNames = searchParams.get('showNames');

  if (searchParams.get('reset')) {
    setQuiz(null);
    window.location.href = window.location.pathname;
  }

  const onStartClick = async () => {
    setIsCreatingQuiz(true);

    try {
      const quiz = await QuizService.createQuiz(searchParams.get('limit') || 100);
      setIsCreatingQuiz(false);
      setQuiz(quiz);
    } catch (e) {
      setCreateError("There was an error initialing the quiz");
    }
  };

  if (!quiz) {
    return (<WelcomePage
      setQuiz={setQuiz}
      onStartClick={onStartClick}
      error={createError}
      isLoading={isCreatingQuiz}
    />);
  }

  return (<QuizForm quiz={quiz} setQuiz={setQuiz} showNames={showNames} />);
}

export default App;
