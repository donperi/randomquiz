import React from 'react';
import Select from 'react-select';

function QuizForm ({ quiz, setQuiz, showNames }) {
  const [selectedName, setSelectedName] = React.useState(null);
  const [error, setError] = React.useState();
  const nextButton = React.useRef({ focus: () => {} });
  const selectElement = React.useRef( { focus: () => {} });

  const currentQuestion = quiz.getCurrentQuestion();

  const isFailed = quiz.isFailed();
  const isComplete = quiz.isComplete();
  const isPassed = quiz.isPassed();

  const handleNext = () => {
    if (!selectedName) {
      setError("Please select an option.");
      return;
    }

    setError(null);

    setQuiz({
      ...quiz,
      position: (quiz.position + 1) < quiz.questions.length ? quiz.position + 1 : quiz.position,
      answers: {
        ...quiz.answers,
        [currentQuestion.id]: currentQuestion.id.toString() === selectedName.value.toString()
      }
    });
    setSelectedName(null);
    selectElement.current.focus();
  }

  const handleReset = () => {
    setQuiz(null);
  }

  return (
    <div className="Quiz container mt-5 w-">
      <h1 className="text-center mb-5">FXFamily Member Quiz</h1>

      <div className="progress mb-2">
        <div
          className={`progress-bar ${isPassed ? 'bg-success' : ''} ${isFailed ? 'bg-danger' : ''}`}
          style={{ width: `${quiz.getCompletionPercent()}%` }}
          role="progressbar"
          aria-valuenow={quiz.position+1}
          aria-valuemin="0"
          aria-valuemax="50"
        />
      </div>

      <div className="card">
        <div className="card-header">
          { isComplete ? 'Completed' : `Question ${quiz.position + 1} out of ${quiz.questions.length}` }
        </div>
        <div className="card-body text-center">
          {error && <div className="alert alert-danger">{error}</div>}

          {isFailed && (
            <div className="alert alert-danger">
              <p>
                Too many incorrect responses recorded.
                <br />Please try again tomorrow!<br />
                Feel free to use the&nbsp;
                <a href="https://webpagefx.mangoapps.com/sites/peoples/people_directory?limit=20">Mango Directory</a> as a study resource.
              </p>
            </div>
          )}

          {isComplete && isPassed && (
            <div className="alert alert-success">
              <h4>Accuracy Percentage: {quiz.accuracyPercentage()}%</h4>
              <p>
                Great job!<br />You've tested your FXFamily name knowledge and earned 1 FXLearns point.<br />
                Please take a screenshot of this results screen and include when you "Rate + Return" this&nbsp;
                resource here: <a href="https://operationsfx.com/myfx/fxlearns">https://operationsfx.com/myfx/fxlearns</a>
              </p>
            </div>
          )}


          {!isComplete && !isFailed && (
            <>
              <img src={currentQuestion.picture} />
              {showNames && <h5>{currentQuestion.name}</h5>}
            </>
          )}
        </div>

        <div id="question-select" className="m-1">
          {!isComplete && !isFailed && (
            <Select
              placeholder="Select..."
              options={quiz.getOptions()}
              ref={selectElement}
              onChange={(selected) => {
                setSelectedName(selected);

                if (nextButton.current) {
                  nextButton.current.focus()
                }
              }}s
              value={selectedName}
            />
          )}
        </div>

        { !isComplete && !isFailed && <button ref={nextButton} onClick={handleNext} className="btn-primary m-1">Submit</button> }
        { (isComplete || isFailed) && <button onClick={handleReset} className={`${isFailed ? 'btn-danger' : 'btn-success'} m-1`}>Start Over</button> }
      </div>

    </div>
  );
}

export default QuizForm;
