import React from 'react'
import {render, fireEvent} from '@testing-library/react';
import {Quiz} from "../Models/Quiz";
import QuizForm from "./QuizForm";
import {selectItem} from "../setupTests";

describe('QuizForm', () => {
  it('should add correct answer if correct option is selected.', async () => {
    const setQuiz = jest.fn();

    const quiz = new Quiz({
      users: { 1: { id: 1, name: '1', picture: { large: '1' } }, 2: { id: 2, picture: { large: '2' }}},
      questions: [1,2],
      position: 0,
      answers: {}
    });

    const { container, getByText } = render(<QuizForm quiz={quiz} setQuiz={setQuiz} />);

    await selectItem(container, 'question-select', '1');

    fireEvent.click(getByText('Submit'));

    expect(setQuiz).toBeCalledWith({
      ...quiz,
      position: 1,
      answers: { 1: true }
    });
  });

  it ('should show success message on Complete', async () => {
    const quiz = new Quiz({
      users: {
        1: { id: 1, name: '1', picture: { large: '1' } },
        2: { id: 2, name: '2', picture: { large: '2' } }
      },
      questions: [1,2],
      position: 1,
      answers: { 1: true, 2: true }
    });

    const { container } = render(<QuizForm quiz={quiz} setQuiz={jest.fn()} />);

    expect(container.innerHTML).toMatch(/Great job!/);
    expect(container.innerHTML).toMatch(/Percentage: 100%/);
    expect(container.querySelectorAll('.progress-bar.bg-success')).toHaveLength(1);
    expect(container.querySelector('button.btn-success').innerHTML).toMatch(/start over/i);
  });

  it ('should show failed message if have already 22% of wrong answers', async () => {
    const quiz = new Quiz({
      users: {
        0: { id: 1, name: '1', picture: { large: '1' } },
        1: { id: 2, name: '2', picture: { large: '2' } }
      },
      questions: Array.from(Array(50).keys()),
      position: 0,
      answers: Array(11).fill(false).reduce((c, v, i) => {
        c[i] = false;
        return c;
      }, {})
    });

    const { container, getByText } = render(<QuizForm quiz={quiz} setQuiz={jest.fn()} />);

    expect(container.innerHTML).toMatch(/Too many incorrect responses recorded/);
    expect(container.querySelectorAll('.progress-bar.bg-danger')).toHaveLength(1);
    expect(container.querySelector('button.btn-danger').innerHTML).toMatch(/start over/i);
  });
});
